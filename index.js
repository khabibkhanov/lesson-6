const buttons = document.querySelectorAll('.btn');
const buttonsWrapper = document.querySelector('.buttons-wrapper')

buttons.forEach(button => button.addEventListener("click", function() {
    document.body.style.backgroundColor = button.value;
}));
